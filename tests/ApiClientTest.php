<?php

namespace ApiModule\Tests;
require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using

use ApiModule\ApiClient;
use ApiModule\Model\HttpMethod;


class ApiClientTest
{
    const API_REQUEST_KEY   = 'aaa';
    const API_RESPONSE_KEY  = 'aaa';
    const API_PROJECT_CODE  = 'abcdefghijklmn';
    const API_REQUEST_ID    = 1234;
    const API_MESSAGE_TIME  = 9876;

    const API_URL = 'http://api.hrsys-gateway.localhost';
    const API_URL_ENDPOINT = '/';
    const API_URL_ENDPOINT_EVENTS = '/v1/events';
    const API_URL_ENDPOINT_ACCESS_TOKEN = '/v1/access_token';

    public $headerClient = [
        'Content-Type'  => 'application/json',
        'Accept' => 'application/json'
    ];

    public function __construct()
    {
        print "------START \n";
        $this->testApiRequest();
        print "\n------END";
    }

    public function testApiRequest()
    {
/*        $bodyOptions['header'] = [
            'authorizationHash' => '54347cf410133ad0d7ffb799a87109d71a21b4508b5a2963a6a2ce29d4da923c',
            'messageTime' => 113531,
            'projectCode' => 'abcdefghijklmn',
            'requestId' => 1234
        ];*/
        $bodyOptions['parameters'] = [
            'arrayField' =>
                [
                    0 => 'testValueOfArray1',
                    1 => 'testValueOfArray2',
                ],
            'booleanField' => true,
            'floatField' => '0.00',
            'integerField' => 0,
            'listField' =>
                [
                    'testKey2' => 'testValueOfList2',
                    'testKey1' => 'testValueOfList1',
                    'testNestedArray' =>
                        [
                            0 => 'nestedArray1',
                            1 => 'nestedArray2',
                        ],
                    'testNestedList' =>
                        [
                            'testNestedKey1' => 'nestedListValue1',
                            'testNestedKey2' => 'nestedListValue2',
                        ],
                ],
            'stringField' => 'string'
        ];

        $apiClient = new ApiClient();
     //   $apiClient->setApiUrlClient(ApiClientTest::API_URL);
     //   $apiClient->setHeadersClient($this->headerClient);
        // lub
         //$apiClient = new ApiClient(ApiClientTest::API_URL, $this->headerClient);

        $apiClient = new ApiClient(ApiClientTest::API_URL);
        $apiClient->getAuthorization()->getAuthParams()->setProjectCode(ApiClientTest::API_PROJECT_CODE);
        $apiClient->getAuthorization()->getAuthParams()->setRequestKey(ApiClientTest::API_REQUEST_KEY);
        $apiClient->getAuthorization()->getAuthParams()->setResponseKey(ApiClientTest::API_RESPONSE_KEY);
        $apiClient->getAuthorization()->getAuthParams()->setRequestId(ApiClientTest::API_REQUEST_ID);

        $response = $apiClient->request(ApiClientTest::API_URL_ENDPOINT_EVENTS, HttpMethod::POST, $bodyOptions);
        print_r($response);
    }
}

new ApiClientTest();
