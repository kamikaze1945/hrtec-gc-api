# Hrtec-gateway-client-api
PHP >= 5.6.0

## Wymagane parametry do połaczenia z API
|Key|Type|Description|
|---|---|---|
|projectCode|string||
| apiUrl | string | |
| apiRequest key | string | |
| apiResponse key | string | |
| requestId | string | |


## Uruchomienie api


## Pobranie paczki z prywatnego repozytorium
W pliku composer.json należy dodać informację o nazwie biblioteki (w tagu require) oraz informację o repozytorium skad można pobrać biblioteke (w tagu repositories)
<pre>
{
    "require": {
        "kamikaze1945/hrtec-gc-api": "dev-master"
    },
    "repositories": [
            {
                "type": "git-bitbucket",
                "url" : "https://bitbucket.org/kamikaze1945/hrtec-gc-api"
            },
            {
                "type": "package",
                "package":{
                    "name":"kamikaze1945/hrtec-gc-api",
                    "version": "dev",
                    "source":{
                        "type":"vcs",
                        "url":"https://bitbucket.org/kamikaze1945/hrtec-gc-api",
                        "reference":"dev"
                    }
                }
            }
    ]
}
</pre>
Aby pobrać bibliotekę, wymagane jest instalacja kluczy SSH dla klienta git.
Więcej informacji https://getcomposer.org/doc/05-repositories.md