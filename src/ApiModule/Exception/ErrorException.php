<?php

namespace ApiModule\Exception;

use Exception;

class ErrorException extends Exception
{
    const ERROR_UNAUTORIZE                  = 'Nieautoryzowany dostęp';
    const ERROR_WRONG_TYPE_AUTHORIZATION    = 'Nieautoryzowany dostęp - nieobsługiwany typ autoryzacyjna.';
    const ERROR_WRONG_AUTHORIZATION_HASH    = 'Nieautoryzowany dostęp - niezgodny token autoryzacji.';
    const ERROR_EMPTY_AUTHORIZATION_HASH    = 'Błąd - brak ustawionego tokenu autoryzacji.';
    const ERROR_EMPTY_PROJECT_CODE          = 'Błąd - brak ustawionego kodu projektu.';
    const ERROR_EMPTY_REQUEST_ID            = 'Błąd - brak ustawionego indentyfikatora zapytania.';
    const ERROR_EMPTY_MESSAGE_TIME          = 'Błąd - brak ustawionego czasu ważności wiadomości.';
}