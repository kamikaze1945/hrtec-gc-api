<?php

namespace ApiModule\Model;


class StandardAuthorizationHeader
{
    const FIELD_AUTHORIZATION_HASH = 'authorizationHash';
    const FIELD_MESSAGE_TIME       = 'messageTime';
    const FIELD_PROJECT_CODE       = 'projectCode';
    const FIELD_REQUEST_ID         = 'requestId';

    /**
     * @var string
     * @see StandardAuthorizationHeader::FIELD_PROJECT_CODE
     */
    protected $projectCode;

    /**
     * @var string
     * @see StandardAuthorizationHeader::FIELD_AUTHORIZATION_HASH
     */
    protected $authorizationHash;

    /**
     * @var string
     * @see StandardAuthorizationHeader::FIELD_REQUEST_ID
     */
    protected $requestId;

    /**
     * @var int
     * @see StandardAuthorizationHeader::FIELD_MESSAGE_TIME
     */
    protected $messageTime;

    /**
     * @return string
     */
    public function getProjectCode()
    {
        return $this->projectCode;
    }

    /**
     * @param string $projectCode
     * @return $this
     */
    public function setProjectCode($projectCode)
    {
        $this->projectCode = $projectCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getAuthorizationHash()
    {
        return $this->authorizationHash;
    }

    /**
     * @param string $authorizationHash
     * @return $this
     */
    public function setAuthorizationHash($authorizationHash)
    {
        $this->authorizationHash = $authorizationHash;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     * @return $this
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * @return int
     */
    public function getMessageTime()
    {
        return $this->messageTime;
    }

    /**
     * @param int $messageTime
     * @return $this
     */
    public function setMessageTime($messageTime)
    {
        $this->messageTime = $messageTime;

        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return [
            self::FIELD_PROJECT_CODE       => $this->getProjectCode(),
            self::FIELD_AUTHORIZATION_HASH => $this->getAuthorizationHash(),
            self::FIELD_REQUEST_ID         => $this->getRequestId(),
            self::FIELD_MESSAGE_TIME       => $this->getMessageTime()
        ];
    }
}