<?php

namespace ApiModule\Model;


use ApiModule\Request\InterfaceRequest;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Exception;

abstract class AbstractClient
{
    /**
     * @var AuthorizeType
     */
    protected $typeAuthorization = AuthorizeType::TYPE_STANDARD;

    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient = null;

    /**
     * @var string
     */
    protected $apiUrlClient;


    /**
     * @var array
     */
    protected $headersClient = [
        InterfaceRequest::HEADER_CONTENT_TYPE  => InterfaceRequest::FORMAT_JSON,
        InterfaceRequest::HEADER_ACCEPT => InterfaceRequest::FORMAT_JSON
    ];

    /**
     * AbstractClient constructor.
     * @param string $apiUrlClient
     * @param array $headersClient
     */
    public function __construct($apiUrlClient = null, $headersClient = null)
    {
        if (isset($apiUrlClient)) {
            $this->apiUrlClient = $apiUrlClient;
        }

        if (isset($headersClient)) {
            $this->headersClient = $headersClient;
        }
    }

    /**
     * @return AuthorizeType
     */
    public function getTypeAuthorization()
    {
        return $this->typeAuthorization;
    }

    /**
     * @param AuthorizeType $typeAuthorization
     */
    public function setTypeAuthorization($typeAuthorization)
    {
        AuthorizeType::isAuthorizeTypeImplemented($typeAuthorization);
        $this->typeAuthorization = $typeAuthorization;
    }


    /**
     * @return string
     */
    public function getApiUrlClient()
    {
        return $this->apiUrlClient;
    }

    /**
     * @param string $apiUrlClient
     * @return $this
     */
    public function setApiUrlClient($apiUrlClient)
    {
        $this->apiUrlClient = $apiUrlClient;
        return $this;
    }

    /**
     * @return array
     */
    public function getHeadersClient()
    {
        return $this->headersClient;
    }

    /**
     * @param array $headersClient
     * @return $this
     */
    public function setHeadersClient($headersClient)
    {
        $this->headersClient = $headersClient;
        return $this;
    }

    /**
     * @return object Client
     */
    public function getHttpClient()
    {
        if (!$this->httpClient) {
            $this->httpClient = new Client(
                [
                    'base_uri' => $this->getApiUrlClient(),
                    'headers'  => $this->getHeadersClient()
                ]
            );
        }

        return $this->httpClient;
    }

    /**
     * @param string $path
     * @param string $method
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function requestClient($path, $method = HttpMethod::GET, $data = []) {

        $bodyRequest = $this->parseOptionsData($method, $data);
        $responseData = [];
        try {
            $response = $this->getHttpClient()->request($method, $path, $bodyRequest);

          //  print_r($response);
            //todo: dodać obsługe response po otrzymaniu odpowiedzi z API
            $responseData = json_decode($response->getBody());

        } catch (RequestException $e) {
            return $e->getMessage();
        } catch (\Exception $e) {
            throw new Exception($e->getMessage());
        }
        return $responseData;
    }

    /**
     * @param string $method
     * @param array $data
     * @return array
     */
    public function parseOptionsData($method, $data = [])
    {
        $bodyRequest = [];
        switch ($method) {
            case HttpMethod::GET:
                if (!empty($data) && is_array($data)) {
                    $query = [];
                    foreach ($data as $key => $value) {
                        $query[$key] = $value;
                    }
                    $bodyRequest['filters'] = $query;
                }
                if (!empty($data) && is_string($data)) {
                    $bodyRequest['query'] = ["filters" => $data];
                }
                break;
            case HttpMethod::POST:
            case HttpMethod::PUT:
            case HttpMethod::PATCH:
                if (!empty($data)) {
                    $json = [];
                    if (is_array($data)) {
                        foreach ($data as $key => $value) {
                            $json[$key] = $value;
                        }
                    }
                    $bodyRequest['json'] = $json;
                }
                break;
        }

        return $bodyRequest;
    }
}