<?php

namespace ApiModule\Model;


class AuthorizeType
{
    const TYPE_STANDARD = 'STANDARD';

    const IMPLEMENTED_AUTHORIZE_TYPES = [
        self::TYPE_STANDARD
    ];

    /**
     * @param string $algorithm
     * @return bool
     */
    public static function isAuthorizeTypeImplemented($type)
    {
        return in_array(mb_strtoupper($type), self::IMPLEMENTED_AUTHORIZE_TYPES);
    }
}