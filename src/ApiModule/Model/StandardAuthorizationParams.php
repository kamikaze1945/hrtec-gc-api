<?php

namespace ApiModule\Model;


class StandardAuthorizationParams
{
    const FIELD_KEY_REQUEST        = 'requestKey';
    const FIELD_KEY_RESPONSE       = 'responseKey';

    /**
     * @var string
     * @see AbstractHeaderModel::FIELD_PROJECT_CODE
     */
    protected $projectCode;

    /**
     * @var string
     * @see StandardAuthorizationParams::FIELD_KEY_RESPONSE
     */
    protected $responseKey;

    /**
     * @var string
     * @see StandardAuthorizationParams::FIELD_KEY_REQUEST
     */
    protected $requestKey;


    /**
     * @var string
     * @see AbstractHeaderModel::FIELD_REQUEST_ID
     */
    protected $requestId;

    /**
     * @return string
     */
    public function getProjectCode()
    {
        return $this->projectCode;
    }

    /**
     * @param string $projectCode
     * @return $this
     */
    public function setProjectCode($projectCode)
    {
        $this->projectCode = $projectCode;

        return $this;
    }

    /**
     * @return string
     */
    public function getRequestId()
    {
        return $this->requestId;
    }

    /**
     * @param string $requestId
     * @return $this
     */
    public function setRequestId($requestId)
    {
        $this->requestId = $requestId;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getRequestKey()
    {
        return $this->requestKey;
    }

    /**
     * @param mixed $requestKey
     */
    public function setRequestKey($requestKey)
    {
        $this->requestKey = $requestKey;
    }

    /**
     * @return mixed
     */
    public function getResponseKey()
    {
        return $this->responseKey;
    }

    /**
     * @param mixed $responseKey
     */
    public function setResponseKey($responseKey)
    {
        $this->responseKey = $responseKey;
    }


    public function toArray()
    {
        return [
            AbstractHeaderModel::FIELD_PROJECT_CODE  => $this->getProjectCode(),
            AbstractHeaderModel::FIELD_REQUEST_ID    => $this->getRequestId(),
            self::FIELD_KEY_REQUEST                  => $this->getRequestKey(),
            self::FIELD_KEY_RESPONSE                 => $this->getResponseKey()
        ];
    }
}