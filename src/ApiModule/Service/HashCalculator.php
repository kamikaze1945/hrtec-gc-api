<?php

namespace ApiModule\Service;


interface HashCalculator
{
    const ALGORITHM_MD5 = 'md5';
    const ALGORITHM_SHA256 = 'sha256';
    const ALGORITHM_SHA512 = 'sha512';

    const IMPLEMENTED_ALGORITHMS = [
        self::ALGORITHM_MD5,
        self::ALGORITHM_SHA256,
        self::ALGORITHM_SHA512
    ];

    /**
     * @param array $data
     * @return mixed
     */
    public function sort(&$data);

    /**
     * @param array $data
     * @param string $plainHash
     * @param string $separator
     * @return mixed
     */
    public function toString($data, &$plainHash, $separator);

    /**
     * @param array $data
     * @param array $additionalData
     * @param string $algorithm
     * @param string $separator
     * @param string $type(request/ressponse)
     * @return string
     * @throws UnauthorizedRequestException
     */
    public function calculate($data, $additionalData = [], $algorithm = self::ALGORITHM_SHA256,  $separator = '|', $type);

    /**
     * @param string $algorithm
     * @return bool
     */
    public function isAlgorithmImplemented($algorithm);
}