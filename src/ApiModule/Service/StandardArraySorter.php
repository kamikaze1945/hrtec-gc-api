<?php

namespace ApiModule\Service;


class StandardArraySorter implements ArraySorter
{
    /**
     * @param array $data
     */
    public function sort(&$data)
    {
        if (empty($data)) {
            return;
        }

        ksort($data);

        foreach ($data as $key => $value) {
            if (is_array($data[$key])) {
                $this->sort($data[$key]);
            }
        }
    }
}