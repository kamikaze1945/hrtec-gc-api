<?php

namespace ApiModule\Service;


use ApiModule\Exception\ErrorException;
use ApiModule\Model\StandardAuthorizationHeader;
use ApiModule\Model\StandardAuthorizationParams;

class StandardAuthHashCalculator implements HashCalculator
{
    const FIELD_REQUEST_DATA  = 'parameters';
    const FIELD_RESPONSE_DATA = 'data';

    /**
     * @var ArraySorter
     */
    private $sorter;

    /**
     * @var ArrayToStringConverter
     */
    private $toStringConverter;

    /**
     * @var StandardAuthorizationParams
     */
    private $standardAuthParams;

    /**
     * StandardHashCalculator constructor.
     * @param ArraySorter $arraySorter
     * @param ArrayToStringConverter $arrayToStringConverter
     */
    public function __construct($arraySorter, $arrayToStringConverter, $standardAuthParams)
    {
        $this->sorter = $arraySorter;
        $this->toStringConverter = $arrayToStringConverter;
        $this->standardAuthParams = $standardAuthParams;
    }

    /**
     * @param array $data
     */
    public function sort(&$data)
    {
        $this->sorter->sort($data);
    }

    /**
     * @param array $data
     * @param string $plainHash
     * @param string $separator
     */
    public function toString($data, &$plainHash, $separator)
    {
        $this->toStringConverter->convert($data, $plainHash, $separator);
    }

    /**
     * @param array $data
     * @param array $additionalData
     * @param string $algorithm
     * @param string $separator
     * @param string $type (request/response)
     * @return string
     * @throws UnauthorizedRequestException
     */
    public function calculate($data, $additionalData = [], $algorithm = self::ALGORITHM_SHA256,  $separator = '|', $type = StandardAuthorizationParams::FIELD_KEY_REQUEST)
    {

        if (!$this->isAlgorithmImplemented($algorithm)) {
            throw new \Exception(ErrorException::ERROR_UNAUTORIZE);
        }

        $this->sort($data);

        $plainHash = $additionalData[StandardAuthorizationHeader::FIELD_PROJECT_CODE] . $separator;
        $plainHash .= $additionalData[StandardAuthorizationHeader::FIELD_REQUEST_ID] . $separator;
        $plainHash .= $additionalData[StandardAuthorizationHeader::FIELD_MESSAGE_TIME] . $separator;

        $dataToHash =
            isset($data[self::FIELD_REQUEST_DATA])
                ? $data[self::FIELD_REQUEST_DATA]
                : (
            isset($data[self::FIELD_RESPONSE_DATA])
                ? $data[self::FIELD_RESPONSE_DATA]
                : []
            );

        $this->toString($dataToHash, $plainHash, $separator);

        switch($type) {
            case StandardAuthorizationParams::FIELD_KEY_RESPONSE:
                $plainHash .= $this->standardAuthParams->getResponseKey();
                break;
            default:
                $plainHash .= $this->standardAuthParams->getRequestKey();
                break;
        }

        return hash($algorithm, $plainHash);
    }

    /**
     * @param string $algorithm
     * @return bool
     */
    public function isAlgorithmImplemented($algorithm)
    {
        return in_array(mb_strtolower($algorithm), self::IMPLEMENTED_ALGORITHMS);
    }

}