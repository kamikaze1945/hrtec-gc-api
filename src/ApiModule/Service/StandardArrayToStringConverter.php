<?php

namespace ApiModule\Service;


class StandardArrayToStringConverter implements ArrayToStringConverter
{
    /**
     * @param array $data
     * @param string $string
     * @param string $separator
     */
    public function convert($data = [], &$string, $separator = '|')
    {
        if (empty($data)) {
            return;
        }

        foreach ($data as $key => $value) {
            if (is_array($data[$key])) {
                $this->convert($data[$key], $string, $separator);
            } else {
                if (!is_null($data[$key])) {
                    if (is_bool($data[$key])) {
                        $string .= $data[$key] ? 'true' : 'false';
                    } else {
                        $string .= $data[$key];
                    }

                    $string .= $separator;
                }
            }
        }

    }
}