<?php

namespace ApiModule\Service;


interface ArraySorter
{
    /**
     * @param array $data
     */
    public function sort(&$data);
}