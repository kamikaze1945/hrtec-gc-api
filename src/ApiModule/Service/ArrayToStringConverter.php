<?php

namespace ApiModule\Service;


interface ArrayToStringConverter
{
    /**
     * @param array $data
     * @param string $string
     * @param string $separator
     */
    public function convert($data = [], &$string, $separator = '|');
}