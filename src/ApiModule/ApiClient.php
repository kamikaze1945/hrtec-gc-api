<?php

namespace ApiModule;


use ApiModule\Authorization\AuthorizationFactory;
use ApiModule\Exception\ErrorException;
use ApiModule\Model\AbstractClient;
use ApiModule\Model\AuthorizeType;

class ApiClient extends AbstractClient
{

    /**
     * @var AuthorizationFactory
     */
    public $helperAuthorization = null;

    public function __construct($apiUrlClient = null, array $headersClient = null)
    {
        parent::__construct($apiUrlClient, $headersClient);
        $this->helperAuthorization();
    }

    public function helperAuthorization()
    {
        return $this->getAuthorization();
    }

    /**
     * @param string $path
     * @param string $method
     * @param array $options
     * @throws \Exception
     */
    public function request($path, $method, $options)
    {
        // walidacja danych wejściowych
        $bodyOptions = $this->getAuthorization()->validRequest(
            $options
        );

        // zapytanie do api
        $responseClient = $this->requestClient($path, $method, $bodyOptions);

        //zamiana na tablice
        $arrayResponse = json_decode(json_encode($responseClient), true);

        // walidacja danych z odpowiedzi
         $this->getAuthorization()->validResponse(
             $arrayResponse
        );

        return $responseClient;
    }

    /**
     * @return \ApiModule\Authorization\StandardAuthorization
     */
    public function getAuthorization()
    {
        if (!AuthorizeType::isAuthorizeTypeImplemented($this->getTypeAuthorization())) {
            throw new \Exception(ErrorException::ERROR_WRONG_TYPE_AUTHORIZATION);
        }

        if (!$this->helperAuthorization) {
            $this->helperAuthorization = AuthorizationFactory::getAuthorization(
                $this->getTypeAuthorization()
            );
        }

        return $this->helperAuthorization;
    }
}