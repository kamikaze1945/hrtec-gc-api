<?php

namespace ApiModule\Authorization;


use ApiModule\Exception\ErrorException;
use ApiModule\Model\StandardAuthorizationHeader;
use ApiModule\Model\StandardAuthorizationParams;
use ApiModule\Request\StandardRequest;
use ApiModule\Request\StandardRequestValidator;
use ApiModule\Response\StandardResponseValidator;
use ApiModule\Response\StandardResponse;
use ApiModule\Service\StandardArraySorter;
use ApiModule\Service\StandardArrayToStringConverter;
use ApiModule\Service\StandardAuthHashCalculator;
use Exception;

class StandardAuthorization implements InterfaceAuthorization
{
    /**
     * @var StandardAuthorizationParams
     */
    public $standardAuthParam;

    /**
     * @var StandardAuthorizationHeader
     */
    public $headerModel;

    /**
     * @var
     */
    public $requestValidator;

    /**
     * @var
     */
    public $responseValidator;


    public function __construct()
    {
        $this->setHeaderModel();
        $this->setAuthParams();
        $this->setRequestValidator();
    }

    public function getHeaderModel()
    {
        return $this->headerModel;
    }

    /**
     * @return StandardAuthorizationHeader
     */
    public function setHeaderModel()
    {
        $this->headerModel = new StandardAuthorizationHeader();
        return $this;
    }

    /**
     * @return StandardAuthorizationParams
     */
    public function getAuthParams()
    {
        return $this->standardAuthParam;
    }

    /**
     * @param StandardAuthorizationParams
     */
    public function setAuthParams()
    {
        $this->standardAuthParam = new StandardAuthorizationParams();
        return $this;
    }

    /**
     * @return StandardRequestValidator
     *
     */
    public function getRequestValidator()
    {
        return $this->requestValidator;
    }

    /**
     * @param StandardRequestValidator
     */
    public function setRequestValidator()
    {
        $this->requestValidator = new StandardRequestValidator();
    }

    /**
     * @return StandardResponseValidator
     *
     */
    public function getResponseValidator()
    {
        return $this->responseValidator;
    }

    /**
     * @param StandardResponseValidator
     */
    public function setResponseValidator()
    {
        $this->responseValidator = new StandardResponseValidator();
    }

    /**
     * @param array $bodyHeader
     * @return StandardAuthorizationHeader
     */
    public function prepareAuthorizationHeader($bodyHeader)
    {

        if (isset($bodyHeader[StandardAuthorizationHeader::FIELD_AUTHORIZATION_HASH])
            && is_string($bodyHeader[StandardAuthorizationHeader::FIELD_AUTHORIZATION_HASH])
        ) {
            $this->headerModel->setAuthorizationHash($bodyHeader[StandardAuthorizationHeader::FIELD_AUTHORIZATION_HASH]);
        }

        if (isset($bodyHeader[StandardAuthorizationHeader::FIELD_PROJECT_CODE])
            && is_string($bodyHeader[StandardAuthorizationHeader::FIELD_PROJECT_CODE])
        ) {
            $this->headerModel->setProjectCode($bodyHeader[StandardAuthorizationHeader::FIELD_PROJECT_CODE]);
        }


        if (isset($bodyHeader[StandardAuthorizationHeader::FIELD_REQUEST_ID])
            && (is_int($bodyHeader[StandardAuthorizationHeader::FIELD_REQUEST_ID])
                || is_string($bodyHeader[StandardAuthorizationHeader::FIELD_REQUEST_ID]))
        ) {
            $this->headerModel->setRequestId($bodyHeader[StandardAuthorizationHeader::FIELD_REQUEST_ID]);
        }

        if (isset($bodyHeader[StandardAuthorizationHeader::FIELD_MESSAGE_TIME])
            && (is_int($bodyHeader[StandardAuthorizationHeader::FIELD_MESSAGE_TIME])
                || is_string($bodyHeader[StandardAuthorizationHeader::FIELD_MESSAGE_TIME])
            )
        ) {
            $this->headerModel->setMessageTime($bodyHeader[StandardAuthorizationHeader::FIELD_MESSAGE_TIME]);
        } else {
            $this->headerModel->setMessageTime(time());
        }

        return $this->headerModel;
    }

    /**
     * @param array $data
     * @return array
     * @throws Exception
     * @throws \ApiModule\Service\UnauthorizedRequestException
     */
    public function validRequest($data)
    {
        $bodyHeader[StandardAuthorizationHeader::FIELD_PROJECT_CODE] = $this->standardAuthParam->getProjectCode();
        $bodyHeader[StandardAuthorizationHeader::FIELD_REQUEST_ID] = $this->standardAuthParam->getRequestId();

        // przygtowanie danych bodyHeader
        $this->prepareAuthorizationHeader($bodyHeader);

        // wygenerowanie authorizationHash
        $hashCalculate = new StandardAuthHashCalculator(
            new StandardArraySorter(),
            new StandardArrayToStringConverter(),
            $this->standardAuthParam
        );

        $this->headerModel->setAuthorizationHash(
            $hashCalculate->calculate(
                $data,
                $this->headerModel->toArray()
            )
        );

        $this->validateRequest();

        return $this->setFormatBody(
            $this->headerModel->toArray(),
            $data[StandardRequest::FIELD_BODY_PARAMETERS]
        );
    }

    /**
     * @param object $responseData
     * @return mixed
     * @throws Exception
     * @throws \ApiModule\Service\UnauthorizedRequestException
     */
    public function validResponse($responseData)
    {
        $this->setHeaderModel();
        $this->setResponseValidator();

        // przygtowanie danych bodyHeader
        $this->prepareAuthorizationHeader($responseData[StandardResponse::FIELD_BODY_HEADER]);

        // wygenerowanie authorizationHash
        $hashCalculate = new StandardAuthHashCalculator(
            new StandardArraySorter(),
            new StandardArrayToStringConverter(),
            $this->standardAuthParam
        );

        $this->validateResponse(
            //wygenerowaie authorizationHash
            $hashCalculate->calculate(
                $responseData[StandardResponse::FIELD_BODY_DATA],
                $this->headerModel->toArray(),
                StandardAuthHashCalculator::ALGORITHM_SHA256,
                '|',
                StandardAuthorizationParams::FIELD_KEY_RESPONSE
            )
        );

        return $responseData;
    }


    /**
     * @throws Exception
     */
    public function validateRequest()
    {
        if(!$this->requestValidator->isAuthorizationHash($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_AUTHORIZATION_HASH);
        }

        if(!$this->requestValidator->isProjectCode($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_PROJECT_CODE);
        }

        if(!$this->requestValidator->isMessageTime($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_MESSAGE_TIME);
        }

        if(!$this->requestValidator->isRequestId($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_REQUEST_ID);
        }
    }

    /**
     * @throws Exception
     */
    public function validateResponse($authorizationHash)
    {
        if(!$this->responseValidator->isAuthorizationHash($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_AUTHORIZATION_HASH);
        }

        if(!$this->responseValidator->equalAuthorizationHash($this->headerModel, $authorizationHash)) {
            throw new ErrorException(ErrorException::ERROR_WRONG_AUTHORIZATION_HASH);
        }

        if(!$this->responseValidator->isProjectCode($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_PROJECT_CODE);
        }

        if(!$this->responseValidator->isMessageTime($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_MESSAGE_TIME);
        }

        if(!$this->responseValidator->isRequestId($this->headerModel)) {
            throw new ErrorException(ErrorException::ERROR_EMPTY_REQUEST_ID);
        }
    }


    /**
     * Przygotowanie tablicy danych
     * @param array $bodyHeader
     * @param array $bodyParameters
     * @return array
     */
    public function setFormatBody($bodyHeader, $bodyParameters)
    {
        return [
            StandardRequest::FIELD_BODY_HEADER      => $bodyHeader,
            StandardRequest::FIELD_BODY_PARAMETERS  => $bodyParameters
        ];
    }

}