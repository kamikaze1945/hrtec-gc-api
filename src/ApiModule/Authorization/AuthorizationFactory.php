<?php

namespace ApiModule\Authorization;


use ApiModule\Model\AuthorizeType;

class AuthorizationFactory
{

    public static function getAuthorization($typeAuthorization = null)
    {
        switch (strtoupper($typeAuthorization)) {
            case AuthorizeType::TYPE_STANDARD:
                $auth = new StandardAuthorization();
                break;
            default:
                $auth = new StandardAuthorization();
                break;
        }
        return $auth;
    }

}