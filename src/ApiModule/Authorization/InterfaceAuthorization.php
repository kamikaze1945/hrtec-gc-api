<?php

namespace ApiModule\Authorization;


interface InterfaceAuthorization
{
    /**
     * @var array
     */
    public function validRequest($data);

    /**
     * @var
     */
    public function validResponse($data);

    /**
     * @return object
     */
    public function getAuthParams();

    /**
     * @return object
     */
    public function setAuthParams();
}