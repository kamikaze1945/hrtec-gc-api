<?php

namespace ApiModule\Request;


interface InterfaceRequest
{
    const SERVER_HTTP_ACCEPT   = 'HTTP_ACCEPT';
    const SERVER_CONTENT_TYPE  = 'CONTENT_TYPE';

    const HEADER_AUTHORIZATION = 'Authorization';
    const HEADER_CONTENT_TYPE  = 'Content-Type';
    const HEADER_ACCEPT        = 'Accept';

    const FORMAT_JSON          = 'application/json';


}