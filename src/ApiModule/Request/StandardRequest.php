<?php

namespace ApiModule\Request;


class StandardRequest implements InterfaceRequest
{
    const FIELD_BODY_HEADER     = 'header';
    const FIELD_BODY_PARAMETERS = 'parameters';

}