<?php

namespace ApiModule\Response;


class StandardResponseValidator
{

    /**
     * @param HeaderModel
     * @return bool
     */
    public function equalAuthorizationHash($headerModel, $authorizationHash)
    {
        if ($authorizationHash != $headerModel->getAuthorizationHash()) {
            return false;
        }
        return true;
    }

    /**
     * @param HeaderModel
     * @return bool
     */
    public function isAuthorizationHash($headerModel)
    {
        if (!$headerModel->getAuthorizationHash()) {
            return false;
        }
        return true;
    }

    /**
     * @param HeaderModel
     * @return bool
     */
    public function isProjectCode($headerModel)
    {
        if (!$headerModel->getProjectCode()) {
            return false;
        }
        return true;
    }

    /**
     * @param HeaderModel
     * @return bool
     */
    public function isRequestId($headerModel)
    {
        if (!$headerModel->getRequestId()) {
            return false;
        }
        return true;
    }

    /**
     * @param HeaderModel
     * @return bool
     */
    public function isMessageTime($headerModel)
    {
        if (!$headerModel->getMessageTime()) {
            return false;
        }
        return true;
    }
}