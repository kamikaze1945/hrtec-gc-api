<?php

namespace ApiModule\Response;


class StandardResponse
{
    const FIELD_BODY_HEADER             = 'header';
    const FIELD_BODY_DATA               = 'data';
    const FIELD_BODY_ADDITIONAL_DATA    = 'additionalData';
    const FIELD_STATUS                  = 'status';
    const FIELD_STATUS_CODE             = 'statusCode';
    const FIELD_MESSAGE                 = 'message';
}